A aplicação utiliza Spring Boot com a API Jersey para a criação de serviços REST (CepController nesse caso).

O método /cep/service/buscar-logradouro-por-cep recebe como parâmetro, via query string, o cep a ser pesquisado.

Ex: /cep/service/buscar-logradouro-por-cep?cep=12345-000

Foram implementados testes unitários para garantir a correta execução da chamada ao serviço cobrindo todas as regras especificadas e utilizando mocks para o serviço de consulta.

Como informado no documento de especificação, o método de busca de logradouro por cep já existia portanto, o CepService foi implementado apenas para efeitos de teste.

package com.jhonis.cep;

import com.jhonis.cep.exception.CepInvalidoException;
import org.springframework.stereotype.Service;

@Service
class CepService {

    // Método já existente na aplicação, implementado apenas para ser utilizado no mock
    Logradouro buscarLogradouroPorCep(String cep) throws CepInvalidoException {
        if (cep == null || cep.isEmpty()) {
            throw new CepInvalidoException();
        }
        return new Logradouro("123", "Mock Test");
    }
}

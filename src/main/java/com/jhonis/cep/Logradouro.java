package com.jhonis.cep;

public class Logradouro {

    private String cep;
    private String logradouro;

    Logradouro(String cep, String logradouro) {
        this.cep = cep;
        this.logradouro = logradouro;
    }

    public String getCep() {
        return cep;
    }

    public String getLogradouro() {
        return logradouro;
    }
}

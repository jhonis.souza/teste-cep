package com.jhonis.cep;

import com.jhonis.cep.exception.CepInvalidoException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/service")
public class CepController {

    private CepService cepService;

    @Autowired
    public CepController(CepService cepService) {
        this.cepService = cepService;
    }

    @RequestMapping("/buscar-logradouro-por-cep")
    public Logradouro buscarLogradouroPorCep(@RequestParam String cep) throws CepInvalidoException {
        return cepService.buscarLogradouroPorCep(cep);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(CepInvalidoException.class)
    public String cepInvalido() {
        return "CEP inválido";
    }
}

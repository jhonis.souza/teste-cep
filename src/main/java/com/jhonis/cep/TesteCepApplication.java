package com.jhonis.cep;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TesteCepApplication {

    public static void main(String[] args) {
        SpringApplication.run(TesteCepApplication.class, args);
    }
}

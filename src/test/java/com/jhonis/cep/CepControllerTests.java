package com.jhonis.cep;

import com.jhonis.cep.exception.CepInvalidoException;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class CepControllerTests {

    private MockMvc mockMvc;

    @Mock
    private CepService cepService;
    @InjectMocks
    private CepController cepController;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(cepController).build();
    }

    @Test
    public void cepNulo() throws Exception {
        mockMvc.perform(get("/service/buscar-logradouro-por-cep"))
                .andExpect(status().isBadRequest());
        verify(cepService, never()).buscarLogradouroPorCep(null);
        verifyNoMoreInteractions(cepService);
    }

    @Test
    public void cepEmBranco() throws Exception {
        when(cepService.buscarLogradouroPorCep("")).thenThrow(new CepInvalidoException());
        mockMvc.perform(get("/service/buscar-logradouro-por-cep?cep="))
                .andExpect(status().isBadRequest())
                .andExpect(content().string("CEP inválido"));
        verify(cepService, times(1)).buscarLogradouroPorCep("");
        verifyNoMoreInteractions(cepService);
    }

    @Test
    public void cepInvalido() throws Exception {
        String cep = "13847322";
        when(cepService.buscarLogradouroPorCep(cep)).thenThrow(new CepInvalidoException());
        mockMvc.perform(get("/service/buscar-logradouro-por-cep?cep=" + cep))
                .andDo(result -> System.out.println(result.getResponse().getContentAsString()))
                .andExpect(status().isBadRequest())
                .andExpect(content().string("CEP inválido"));
        verify(cepService, times(1)).buscarLogradouroPorCep(anyString());
        verifyNoMoreInteractions(cepService);
    }

    @Test
    public void cepExato() throws Exception {
        String logradouro = "R VER JOAO M BUENO";
        String cep = "13847221";
        when(cepService.buscarLogradouroPorCep(cep)).thenReturn(new Logradouro(cep, logradouro));
        mockMvc.perform(get("/service/buscar-logradouro-por-cep?cep=" + cep))
                .andExpect(status().is2xxSuccessful())
                .andExpect(jsonPath("$.cep", is(cep)))
                .andExpect(jsonPath("$.logradouro", is(logradouro)));
        verify(cepService, times(1)).buscarLogradouroPorCep(anyString());
        verifyNoMoreInteractions(cepService);
    }

    @Test
    public void cepComUltimoDigitoIncorreto() throws Exception {
        String logradouro = "R CNSO JOAQUIM M SALGADO";
        String cepRequest = "13847225";
        String cepResponse = "13847220";
        when(cepService.buscarLogradouroPorCep(cepRequest)).thenReturn(new Logradouro(cepResponse, logradouro));
        mockMvc.perform(get("/service/buscar-logradouro-por-cep?cep=" + cepRequest))
                .andExpect(status().is2xxSuccessful())
                .andExpect(jsonPath("$.cep", is(cepResponse)))
                .andExpect(jsonPath("$.logradouro", is(logradouro)));
        verify(cepService, times(1)).buscarLogradouroPorCep(anyString());
        verifyNoMoreInteractions(cepService);
    }

    @Test
    public void cepComTresUltimosDigitosIncorretos() throws Exception {
        String logradouro = "AV MORUMBI";
        String cepRequest = "13572999";
        String cepResponse = "13572000";
        when(cepService.buscarLogradouroPorCep(cepRequest)).thenReturn(new Logradouro(cepResponse, logradouro));
        mockMvc.perform(get("/service/buscar-logradouro-por-cep?cep=" + cepRequest))
                .andExpect(status().is2xxSuccessful())
                .andExpect(jsonPath("$.cep", is(cepResponse)))
                .andExpect(jsonPath("$.logradouro", is(logradouro)));
        verify(cepService, times(1)).buscarLogradouroPorCep(anyString());
        verifyNoMoreInteractions(cepService);
    }
}
